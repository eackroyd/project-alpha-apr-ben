from django.urls import path
from accounts.views import AccountLogin, AccountLogout, AccountSignup

urlpatterns = [
    path("login/", AccountLogin, name="login"),
    path("logout/", AccountLogout, name="logout"),
    path("signup/", AccountSignup, name="signup"),
]
